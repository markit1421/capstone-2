const Order = require('../models/Order');
const User = require('../models/User');
const Product = require('../models/Product');
const auth = require('../auth');

//[SECTION] Functionality [CREATE]
module.exports.createOrder = async (req, res) => {
    if(parseInt(req.body.quantity)<=0){
        return res.send('Invalid order number!');
    }
    let address=req.body.shippingAddress;
    let contact=req.body.contactNumber;

    if (!address.trim() || !contact.trim()){
        return res.send('Shipping Address and Contact Number are required!');
    }

    let prodPrice;
    let qty;
   let isOrderAdded= await Product.findById(req.body.orderItem).then(
        product =>{
        qty=product.stock;
        if(product.stock===0){
            return res.send('No more stocks available!');
        }else if(product.stock<req.body.quantity){
            return res.send(`Only ${product.stock} units of the product are available. Try Again!`);
        }else{

        prodPrice=parseInt(product.price);
        
        let newOrder=new Order({
            orderItem: req.body.orderItem,
            quantity: req.body.quantity,
            totalAmount: prodPrice*parseInt(req.body.quantity),
            user: req.user.id,
            shippingAddress: req.body.shippingAddress,
            contactNumber: req.body.contactNumber
        })       
         return newOrder.save().then(order => true).catch(err => err.message)
        }
    });

    if(isOrderAdded !== true) {
      return res.send({message: isOrderAdded})
    }

    let newStock=parseInt(qty)-req.body.quantity;

    let isProdStockUpdated = await Product.findByIdAndUpdate(req.body.orderItem,{stock: newStock}).then(
        prod => true).catch(err => err.message);

    if(isProdStockUpdated !== true) {
      return res.send({message: isProdStockUpdated})
    }


    if(isOrderAdded && isProdStockUpdated) {
      return res.send("Order succesfully added.")
  }
};

//[SECTION] Functionality [RETRIEVE]
module.exports.userOrders = async (req, res) => {
    Order.find({user: req.user.id}).then(resultOrders => {
        return res.send(resultOrders);
    });
};

module.exports.singleOrder = async (req, res) => {
    Order.find({$and:[{_id:req.params.id},{user: req.user.id}]}).then(resultOrder => {
        return res.send(resultOrder);           
   })
};

module.exports.getOrders =  async (req, res) => {
    Order.find({}).then(resultsO => {
        return res.send(resultsO);
    });
};

//[SECTION] Functionality [UPDATE]
module.exports.updateOrder = async (req, res) => {     
    Order.findByIdAndUpdate(req.params.id,{orderStatus: req.body.status}).then((orderUpdated,err)=>{
        if (err) {
            return res.send('Error updating order.');
        } else {
            return res.send("Order was successfully updated.");
        }
    }).catch(err => res.send(err));    
}

//[SECTION] Functionality [DELETE]
module.exports.deleteOrder = async (req, res) => {     
    Order.findByIdAndRemove(req.params.id).then((orderDeleted,err)=>{
        if (err) {
            return res.send('Error deleting order.');
        } else {
            return res.send("Order was successfully deleted.");
        }
    }).catch(err => res.send(err));    
}