// Dependencies and Modules
const mongoose = require('mongoose');


// Blueprint schema
const orderSchema = new mongoose.Schema({
   orderItem: {
        type: String,
        required: true
    },
    quantity: {
        type: Number,
        default: 1
    },
    totalAmount: {
        type: Number,
        required: true
    },
    user: {
        type: String,
        required: true
    },  
    shippingAddress: {
        type: String,
        required: [true, 'Shipping Address is required']
    },
    contactNumber: {
        type: String,
        required: [true, 'Contact Number is required']
    },
    orderStatus:{
         type: String,
         default: 'processing'
    },
    createdAt: {
        type: Date,
        default: new Date()
    }
});


// Model
const Order = mongoose.model("Order", orderSchema);
module.exports = Order;