//[SECTION] Dependencies and Modules
const exp = require("express");
const controller = require('../controllers/orders');
const auth = require('../auth');

//Destructure verify from auth
const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
const route = exp.Router();

//[SECTION] [POST] Routes
route.post('/new', verify, controller.createOrder);

//[SECTION] [GET] Routes
route.get('/me', verify, controller.userOrders);

route.get('/single/:id', verify, controller.singleOrder);

route.get('/all', verify,verifyAdmin, controller.getOrders);

//[SECTION] [PUT] Routes
route.put('/update/:id', verify,verifyAdmin, controller.updateOrder);

//[SECTION] [DEL] Routes
route.delete('/delete/:id', verify,verifyAdmin, controller.deleteOrder);




//[SECTION] Export Route System
module.exports = route;