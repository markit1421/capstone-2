// Dependencies and Modules
const mongoose = require('mongoose');


// Blueprint schema
const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Product Name is required']
    },
    description: {
        type: String,
        required: [true, 'Product Description is required']
    },
    price: {
        type: Number,
        required: [true, 'Product Price is required']
    },
    ratings: {
        type: Number,
        default: 0
    },
    category: {
        type: String,
        required: [true, 'Product Category is required']
    },
     stock: {
        type: Number,
        default: 0
    },
     numOfReviews: {
        type: Number,
        default: 0
    },
    createdBy: {
        type: String,
        required: true
    },
    reviews:[{
        user: {
            type: String,
            required: true
        },
        rating: {
            type: Number,
            default: 0
        },
        comment: {
            type: String,
            required: true
        }
    }],
    createdAt: {
        type: Date,
        default: new Date()
    },
    status:{
        type: String,
        default: 'available'
    }
});


// Model
const Product = mongoose.model("Product", productSchema);
module.exports = Product;