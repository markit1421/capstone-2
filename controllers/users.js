//[SECTION] Dependencies and Modules
const User = require('../models/User');
const auth = require('../auth');
const bcrypt = require('bcrypt');
const dotenv = require('dotenv');
dotenv.config();
const salt = parseInt(process.env.SALT);

//[SECTION] Functionality [CREATE]
module.exports.registerUser = async (req, res) => {
    let nme= req.body.name;
    let eml = req.body.email;
    let pw = req.body.password;
    
    if (nme.trim() == '' || eml.trim() == '' || pw.trim() == '') {
        return res.send('All fields are required.');
    } 

    User.findOne({email:eml}).then (userFound =>{
       if (userFound != null && userFound.email == eml){
            return res.send('Email already existed, try again!');
            }
       else{
            let newUser = new User({
            name: nme,
            email: eml,
            password: bcrypt.hashSync(pw, salt)
        })
            return newUser.save().then((savedUser, error) => {
            if (error) {
                return res.send('Failed to register new user');
            } else {
                return res.send(savedUser);
            }
        })
        .catch(err => res.send(err));
       }

    })
    .catch(err => res.send(err));
 
};


module.exports.loginUser = (req, res) => {

    User.findOne({
            email: req.body.email
        })
        .then(result => {
            if (result === null) {
                return res.send('User Not Found');
            } else {
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
               
                if (isPasswordCorrect) {
                    return res.send({
                        accessToken: auth.createAccessToken(result)
                    });
                } else {
                    return res.send("Incorrect Password");
                }
            }
        })
        .catch(err => res.send(err));
};

//[SECTION] Functionality [RETRIEVE]
module.exports.userDetails = async (req, res) => {
    User.findById(req.user.id,'name email').then(resultUserDetails => {
        return res.send(resultUserDetails);
    });
};

module.exports.getUsers = async (req, res) => {
    User.find({isAdmin: false}).then(resultUserDetails => {
        return res.send(resultUserDetails);
    });
};

module.exports.getSingleUser = async (req, res) => {
    User.findById(req.params.id).then(resultOneUser => {
        return res.send(resultOneUser);
    });
};

//[SECTION] Functionality [UPDATE]
module.exports.userDetailsUpdate = async (req, res) => {
    let userN= req.body.name;
    let eml = req.body.email;
   
    let updatedUser = {
          name: userN,
          email: eml
    }
    
    if (!userN.trim() || !eml.trim() ){
        return res.send('Invalid Inputs.');
    } 

    User.findOne({email: eml}).then (userFound =>{
       if (userFound!==null && userFound.id !== req.user.id && userFound.email == eml){
            return res.send('Email already existed, try again!');
        }else{
            User.findByIdAndUpdate(req.user.id, updatedUser).then((userUpdated, err) => {
                if (err) {
                    return res.send('Error updating user.');
                } else {
                    return res.send('User profile succesfully updated.');
                }
            })
        .catch(err => res.send(err));
       }

    }).catch(err => res.send(err));  
}

module.exports.userPasswordUpdate = async (req, res) => {
    let oldPW= req.body.oldPassword;
    let newPW= req.body.newPassword;
    let confirmPW= req.body.confirmPassword;
   
    let updatedUserPW = {password: bcrypt.hashSync(confirmPW, salt)}
    
    if (!oldPW.trim() || !newPW.trim() || !confirmPW.trim()){
        return res.send('Invalid Inputs.');
    } 

    User.findById(req.user.id).then (userFound =>{
        const isPasswordCorrect = bcrypt.compareSync(oldPW, userFound.password);

        if(isPasswordCorrect){
            if(newPW !== confirmPW){
                return res.send('Password not match!');
            }else{
                User.findByIdAndUpdate(req.user.id, updatedUserPW).then((userUpdated, err) => {
                if (err) {
                    return res.send("Error updating user's password.");
                } else {
                    return res.send("User's password succesfully updated.");
                }
            }).catch(err => res.send(err));
            }
        }else{
             return res.send('Enter your correct Old Password');
        }
    
            
    }).catch(err => res.send(err));  
}

module.exports.changeRole = async (req, res) => {
     User.findById(req.params.id).then(resultOneUser => {
        let stat;
        if(resultOneUser.isAdmin){stat=false;}
        else{stat=true;}
            User.findByIdAndUpdate(req.params.id,{isAdmin: stat}).then((userRole,err)=>{
                if (err) {
                    return res.send('Error changing role.');
                } else {
                    return res.send("Role was successfully changed.");
                }
            }).catch(err => res.send(err)); 
    });    
}

module.exports.deactivateUser = async (req, res) => {
     User.findById(req.params.id).then(resultOneUser => {
        if(resultOneUser.isActive==false){
            return res.send('This user has already been deactivated.');
        }else{
            User.findByIdAndUpdate(req.params.id,{isActive: false}).then((userDeactivated,err)=>{
                if (err) {
                    return res.send('Error deactivating user.');
                } else {
                    return res.send(`User - ${userDeactivated.name} has been deactivated.`);
                }
            }).catch(err => res.send(err));
        }
        
    });    
}

module.exports.activateUser = async (req, res) => {
     User.findById(req.params.id).then(resultOneUser => {
        if(resultOneUser.isActive){
            return res.send('This user has already been activated.');
        }else{
            User.findByIdAndUpdate(req.params.id,{isActive: true}).then((userActivated,err)=>{
                if (err) {
                    return res.send('Error activating user.');
                } else {
                    return res.send(`User - ${userActivated.name} has been activated.`);
                }
            }).catch(err => res.send(err));
        }
        
    });    
}
//[SECTION] Functionality [DELETE]