//[SECTION] Dependencies and Modules
const exp = require("express");
const controller = require('../controllers/products');
const auth = require('../auth');

//Destructure verify from auth
const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
const route = exp.Router();

//[SECTION] [POST] Routes
route.post('/new', verify, verifyAdmin, controller.createProduct);


//[SECTION] [GET] Routes
route.get('/all',  controller.allProducts);

route.get('/product/:id',  controller.singleProduct);

route.get('/reviews/:id', controller.singleProductReviews);


//[SECTION] [PUT] Routes
route.put('/product/:id', verify,verifyAdmin, controller.updateProduct);

route.put('/archive/:id', verify,verifyAdmin, controller.archivedProduct);

route.put('/restore/:id', verify,verifyAdmin, controller.restoreProduct);

route.put('/review', verify, controller.reviewProduct);


route.delete('/review/remove', verify,verifyAdmin, controller.deleteReview);

//[SECTION] [DEL] Routes

//[SECTION] Export Route System
module.exports = route;
