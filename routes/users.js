//[SECTION] Dependencies and Modules
const exp = require("express");
const controller = require('../controllers/users');
const auth = require('../auth');

//Destructure verify from auth
const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
const route = exp.Router();

//[SECTION] [POST] Routes
route.post('/register', controller.registerUser);

route.post('/login', controller.loginUser);

//[SECTION] [GET] Routes
route.get('/me', verify, controller.userDetails);

route.get('/all', verify, verifyAdmin, controller.getUsers);

route.get('/singleuser/:id', verify, verifyAdmin, controller.getSingleUser);

//[SECTION] [PUT] Routes
route.put('/profile/update', verify, controller.userDetailsUpdate);

route.put('/password/update', verify, controller.userPasswordUpdate);

route.put('/changerole/:id', verify, verifyAdmin, controller.changeRole);

route.put('/deactivate/:id', verify, verifyAdmin, controller.deactivateUser);

route.put('/activate/:id', verify, verifyAdmin, controller.activateUser);


//[SECTION] [DEL] Routes


//[SECTION] Export Route System
module.exports = route;